import org.apache.spark.ml.clustering.{BisectingKMeans, KMeans}
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.DoubleType

object UnsupervisedML {

  /**
   * Method that used to calculate F1 score for K-Means algorithm in unsupervised phase of the programm.
   * All features assemble in one column to pass them in the algorithm.
   *
   * @param dataFrame Contains the features that will train the model
   * @param features Column names of the dataframe to be used as features in the model
   * @return Double number with F1 score
   */
  def getKmeansScore(dataFrame: DataFrame, features: Array[String]): Double ={
    val data = Utilities.assembler(dataFrame, features)

    val k_means = new KMeans().setK(2).setSeed(1L).setFeaturesCol("featuresFinal")
    val model = k_means.fit(data)

    val predictions = model.transform(data)

    val f1 = evaluation(predictions)
    f1
  }

  /**
   *
   *
   * @param dataFrame Data with features to train the model
   * @param features Names of the columns to be used as features
   */
  def bisectingKMeans(dataFrame: DataFrame, features: Array[String]): Double ={
    val data = Utilities.assembler(dataFrame, features)

    val bkm = new BisectingKMeans()
    val model = bkm
      .setK(2)
      .setSeed(1)
      .setFeaturesCol("featuresFinal")
      .fit(data)
    val predictions = model.transform(data)
    val f1 = evaluation(predictions)
    f1
  }

  def evaluation(data: DataFrame): Double ={
    val predictions = data.withColumn("prediction_d", col("prediction").cast(DoubleType))
    val evaluator = new MulticlassClassificationEvaluator()
      .setLabelCol("class")
      .setPredictionCol("prediction_d")

    val f1 = evaluator.evaluate(predictions)
    f1
  }
}
