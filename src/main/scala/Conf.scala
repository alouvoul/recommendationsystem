import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{DataFrame, SQLContext, SparkSession}

object Conf {
  /**
   * If this set to True in each step data are printed with a message.
   */
  val DEBUG = true

  /**
   * Values that declares how many row od the data need to be printed. Data shown in array format.
   */
  val numberOfRowsToPrint = 5

  /**
   * If value is 0 whole files will be read. Otherwise, a limit with this value will be read.
   */
  val FILE_READ_LIMIT = 0

  /**
   * Sets the percentage of training data for Machine learning algorithms
   */
  val trainDataPercentage = 0.6

  /**
   * Sets the percentage of testing data to evaluate algorithms results.
   */
  val testDataPercentage = 0.4

  /**
   *
   */
  var filename_for_preprocessing_save_results:String = "final_data" + "_" + FILE_READ_LIMIT + ".parquet"

  /**
   * Method that prints information in console for debugging
   *
   * @param df A dataframe that will be shown in console
   * @param phase A keyword to preview
   */
  def debug(df: DataFrame, phase: String): Unit ={
    if(DEBUG){
      println("=================== "+phase+" ===================")
      df.show(numberOfRowsToPrint,false)
      println()
    }
  }

  /**
   * Method that initialize a SPARK project with the configuration.
   *
   * @return
   */
  def sparkConfigurations(): SQLContext ={
    val sparkConf = new SparkConf()
      .setMaster("local[4]")
      .setAppName("Big Data Project")
    val sc = new SparkContext(sparkConf) // create spark context
    sc.setLogLevel("ERROR")
    val spark = SparkSession.builder().getOrCreate().sqlContext
    return spark
  }
}
