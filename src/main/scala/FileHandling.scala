import org.apache.spark.sql.{DataFrame, SQLContext}
import org.apache.spark.sql.functions.{col, collect_list, concat, lit}
import org.apache.spark.sql.types.StringType
import java.nio.file.{Paths, Files}

object FileHandling {

  val currentDir = System.getProperty("user.dir") //Get the current dir
  val fileFolder =  currentDir + "\\src\\main\\resources\\"

  /**
   * This function is used to read files in main. In Conf.scala file there is the value to limit the returned dataframe
   * values. This can be used for faster results.
   *
   * @param fileName Name of the file
   * @param spark
   * @return
   */
  def fileRead(fileName:String, spark: SQLContext): DataFrame ={
    val df = spark.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .load(fileFolder+fileName)
    if(Conf.FILE_READ_LIMIT != 0){
      return df.limit(Conf.FILE_READ_LIMIT)
    }
    return df
  }

  def readParquet(fileName: String, spark: SQLContext): DataFrame = {
    val df = spark.read.parquet(fileFolder+fileName)
    if(Conf.FILE_READ_LIMIT != 0){
      return df.limit(Conf.FILE_READ_LIMIT)
    }
    df
  }

  /**
   * This method is used to modify the original attribute file' s schema to another that can be used more efficiently
   * by the application. It basically collects all the attributes of the same product in one row and one column.
   * This process creates excessive punctuation ('[', ']', ',') that doesnt affect the results since it will be
   * removed during the punctuation-removal phase.
   *
   * @param: The name of the file
   * @param: A spark object
   * @return: Modified Dataframe
   */
  def attributeFileRead(fileName: String, spark: SQLContext): DataFrame = {
    var attributeFile = fileRead("attributes.csv", spark)
      .groupBy("product_uid")
      .agg(collect_list("name") as "name_array", collect_list("value") as "value_array")
    attributeFile = attributeFile.withColumn("name", attributeFile.col("name_array").cast(StringType))
    attributeFile = attributeFile.withColumn("value", attributeFile.col("value_array").cast(StringType))
    attributeFile = attributeFile.withColumn("attributes", concat(col("name"), lit(", "), col("value")))
    attributeFile = attributeFile.drop("name_array").drop("value_array")
      .drop("name").drop("value")
    attributeFile
  }

  /**
   *
   *
   * @param dataFrame
   * @param fileName
   */
  def writeFile(dataFrame: DataFrame, fileName:String): Unit ={
    dataFrame.write.parquet(fileFolder+fileName)
  }

  /**
   *
   * @param fileName
   * @return
   */
  def fileExist(fileName: String): Boolean ={
    return Files.exists(Paths.get(fileFolder+fileName))
  }
}
