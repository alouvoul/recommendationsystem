import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.clustering.KMeans
import org.apache.spark.ml.evaluation.{MulticlassClassificationEvaluator, RegressionEvaluator}
import org.apache.spark.ml.feature.{VectorAssembler, VectorIndexer}
import org.apache.spark.ml.regression.{DecisionTreeRegressor, GBTRegressor, LinearRegression, RandomForestRegressor}
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.col


object SupervisedML {

  /**
   *
   * @param dataset: Input dataset for the Algorithm
   * @param featureCol: Name of the column that contains the features
   * @param labelColName: Name of the column that contains the labels
   * @return Mean Squared Error
   */

  def getLinearRegressionMSE(dataset: DataFrame, featureCol: Array[String], labelColName : String): Double = {

    // Label column has to be numeric
    var data = dataset.withColumn("relevance_numeric", dataset.col(labelColName).cast(DoubleType))
    // Rename "relevance" column as "label": Necessary for the algorithm to work
    data = data.withColumnRenamed("relevance_numeric", "label")

    // Features-column has to be array-type
    data = Utilities.assembler(data, featureCol)

    // Split the dataset as the percentage declared in Conf.scala file
    val Array(train, test) = data.randomSplit(Array(Conf.trainDataPercentage, Conf.testDataPercentage), 42)
    val model = new LinearRegression()
      .setFeaturesCol("featuresFinal")

    val lrModel = model.fit(train)
    val predictions = lrModel.transform(test)

    val mse = evaluation(predictions)
    mse
  }

  /**
   * Decision Tree regression algorithm implement and calculates RMSE value to compare with other algorithms. This method
   * called from MainFile.scala file.
   *
   * @param dataFrame
   * @param featureCol
   * @param labelCol
   * @return
   */
  def getDecisionTreeRegressionMSE(dataFrame: DataFrame, featureCol:Array[String], labelCol: String): Double ={

    var data = Utilities.assembler(dataFrame, featureCol)

    val featureIndexer = new VectorIndexer()
      .setInputCol("featuresFinal")
      .setOutputCol("featuresSimilarity")
      .setMaxCategories(4)
      .fit(data)
    data = data.withColumn("relevance", data.col(labelCol).cast(DoubleType))
    // Split the dataset as the percentage declared in Conf.scala file
    val Array(trainingData, testData) = data.randomSplit(Array(Conf.trainDataPercentage, Conf.testDataPercentage),42)
    val dt = new DecisionTreeRegressor()
      .setLabelCol("relevance")
      .setFeaturesCol("featuresSimilarity")
      .setMaxDepth(6)

    val pipeline = new Pipeline()
      .setStages(Array(featureIndexer, dt))
    val model = pipeline.fit(trainingData)
    val predictions = model.transform(testData)

    val mse = evaluation(predictions)
    return mse
  }

  /**
   *
   * @param dataFrame
   * @param featureCol
   * @param labelCol
   * @return
   */
  def getRandomForestRegressionMSE(dataFrame: DataFrame, featureCol: Array[String], labelCol: String): Double ={

    var data = Utilities.assembler(dataFrame, featureCol)

    val featureIndexer = new VectorIndexer()
      .setInputCol("featuresFinal")
      .setOutputCol("featuresSimilarity")
      .setMaxCategories(4)
      .fit(data)

    data = data.withColumn("relevance", data.col(labelCol).cast(DoubleType))
    // Split the dataset as the percentage declared in Conf.scala file
    val Array(trainingData, testData) = data.randomSplit(Array(Conf.trainDataPercentage, Conf.testDataPercentage),42)

    val rf = new RandomForestRegressor()
      .setLabelCol("relevance")
      .setFeaturesCol("featuresSimilarity")
      .setMaxDepth(11)
      .setNumTrees(150)

    val pipeline = new Pipeline()
      .setStages(Array(featureIndexer, rf))
    val model = pipeline.fit(trainingData)
    val predictions = model.transform(testData)

    val mse = evaluation(predictions)
    return mse
  }

  /**
   *
   * @param dataFrame
   * @param featureCol
   * @param labelCol
   * @return
   */
  def getGradientBoostedTreeRegressionMSE(dataFrame: DataFrame, featureCol: Array[String], labelCol: String):Double={
    var data = Utilities.assembler(dataFrame, featureCol)

    val featureIndexer = new VectorIndexer()
      .setInputCol("featuresFinal")
      .setOutputCol("featuresSimilarity")
      .setMaxCategories(4)
      .fit(data)

    data = data.withColumn("relevance", data.col(labelCol).cast(DoubleType))
    // Split the dataset as the percentage declared in Conf.scala file
    val Array(trainingData, testData) = data.randomSplit(Array(Conf.trainDataPercentage, Conf.testDataPercentage),42)

    val gbt = new GBTRegressor()
      .setLabelCol("relevance")
      .setFeaturesCol("featuresSimilarity")
      .setMaxDepth(6)

    val pipeline = new Pipeline()
      .setStages(Array(featureIndexer, gbt))
    val model = pipeline.fit(trainingData)
    val predictions = model.transform(testData)

    val mse = evaluation(predictions)
    return mse
  }

  /**
   * Method that implements evaluation process for each ML algorithm. Used after model transform(predictions) to
   * calculate the MSE error of the results.
   *
   * @param predictions Dataframe with predicted values that each algorithm produce
   * @return Double value that represents the MSE of the model
   */
  def evaluation(predictions:DataFrame): Double ={
    var data = predictions.withColumn("relevance", predictions.col("relevance").cast(DoubleType))
    val evaluator = new RegressionEvaluator()
      .setLabelCol("relevance")
      .setPredictionCol("prediction")
      .setMetricName("mse")

    val mse = evaluator.evaluate(data)
    mse
  }
}
