import org.apache.spark.ml.feature.{HashingTF, IDF, NGram, PCA, StopWordsRemover, Tokenizer, VectorAssembler}
import org.apache.spark.sql.functions._
import org.apache.spark.ml.linalg.SparseVector
import org.apache.spark.mllib.feature.Stemmer
import org.apache.spark.sql.types.{DoubleType, StringType}
import org.apache.spark.sql.{DataFrame, SQLContext}

import scala.collection.mutable

object Utilities {

  /**
   * This method remove punctuations and replace some shortcuts. Each row transformed to
   * String and after all actions happen.
   * IMPORTANT after the conversion process to string, data can not reverse to columns.
   *
   * @param dataFrame column A column of a dataset
   * @param spark Need for transformations
   * @param outCol Used to re-format the data
   * @return Converted Dataframe returned
   */
  def getRemovedPunctuationData(dataFrame: DataFrame, spark: SQLContext, outCol:Array[String]): DataFrame ={
    import spark.implicits._
    val pattern1: scala.util.matching.Regex = """([0-9])([a-z])""".r

    val cleaned: DataFrame = dataFrame.map(
      row => {
        val row1 = row(0).toString
        val row2 = row(1).toString
          .replaceAll("WrappedArray", "") //Needs Optimization
          .replaceAll("""\(""", "")
          .replaceAll("""\)""", "")
          .replaceAll("""\[""", "")
          .replaceAll(""""[(){}\[\]"]"""", "")
          .replaceAll("""\bfoot\b|ft\.\b|ft\b""","feet")
          .replaceAll("""\binch\b|in\.\b""","inches")
          .replaceAll("""([0-9]+) in""", "$1 inches")
          .replaceAll("""\bpounds\b|\blibs\b|\blbs\b|lb\.\b|\blb\b""", "pound")
          .replaceAll("""\bgallons\b|gal\.\b|\bgal\b""", "gallon")
          .replaceAll("""\bounces\b|oz\.\b|\boz\b""","ounce")
          .replaceAll("""cu\.""","cubic")
          .replaceAll("""sq\.""","square")
          .replaceAll("""centimeters|cm\.\b""", "cm")
          .replaceAll("""milimeters|mm\.\b""", "mm")
          .replaceAll("""volts|\bvolt\.\b| v """, "volt")
          .replaceAll("""watts|watt\.""", "watt")
          .replaceAll("""\bamperes\b|\bampere\b|\bamps\b|amp\.""", "amp")
          .replaceAll("""zero\.?""", "0 ")
          .replaceAll("""one\.?""", "1 ")
          .replaceAll("""two\.?""", "2 ")
          .replaceAll("""three\.?""", "3 ")
          .replaceAll("""four\.?""", "4 ")
          .replaceAll("""five\.?""", "5 ")
          .replaceAll("""six\.?""", "6 ")
          .replaceAll("""seven\.?""", "7 ")
          .replaceAll("""eight\.?""", "8 ")
          .replaceAll("""nine\.?""", "9 ")
          .replaceAll("""bullet ([0-9]+)""", " ")
          .replaceAll("""\bh\b""", "height")
          .replaceAll("""\bw\b""", "width")
          .replaceAll("""\b([A-Z][a-z]+)([A-Z]+[a-z]*)\b""", "$1 $2")


          .replaceAll("""\.""","")
          .replaceAll("°", " degrees")
          .replaceAll("#", "")
          .replaceAll(",", "")
          .replaceAll("-", " ")
          .replaceAll("&amp;", "")
          .replaceAll(";", "")
          .replaceAll("&", "")
          .replaceAll("%", "")
          .replaceAll("  ", "")
          .replaceAll("\\$", "")
          .replaceAll("\\?", " ")
          .replaceAll("//", "/")
          .replaceAll("""\.\.""", ".")
          .replaceAll("/", " ")
          .replaceAll(" \\\\ ", " ")
          .replaceAll("""\.""", " . ")
          .replaceAll("""(\.|/)$""", "")
          .replaceAll("""([0-9])([a-z])""", "$1 $2")
          .replaceAll("""([a-z])([0-9])""", "$1 $2")
          .replaceAll(" x "," xbi ")
          .replaceAll("""([0-9]+) ?x ?([0-9]+)""", """$1 xbi $2""")
          .replaceAll("""([0-9]+) ?/ ?([0-9]+)""", """$1 xbi $2""")
          .replaceAll("""([a-z])( *)\.( *)([a-z])""", "$1 $4")
          .replaceAll("""([a-z])( *)/( *)([a-z])""", "$1 $4")
          .replaceAll("""\*""", """ xbi """)
          .replaceAll("""([0-9]+) ?by ?([0-9]+)""", """$1 xbi $2""")
          .replaceAll("""([0-9])( *)\.( *)([0-9])""", "$1. $4")

          //.replaceAll("[a-z|A-Z][0-9]", " ")
        val row3 = row(2).toString
        (row1, row2, row3)
      }).toDF(outCol: _*)

    Conf.debug(cleaned, "Remove Punctuations")
    return cleaned
  }

  /**
   * Function to stem the given dataframe
   *
   * @param dataFrame: Dataframe to be stemmed
   * @param inputCol : Name of column to be stemmed
   * @return Stemmed dataframe
   */
  def getStemmedData(dataFrame: DataFrame, idCol: String, inputCol: String): DataFrame ={
    val x = dataFrame.select(
      col(idCol).as("id_temp"), col(inputCol),
      explode(col(inputCol)).as("terms")
    )
    val stemmer = new Stemmer()
      .setInputCol("terms")
      .setOutputCol("terms" + "_stemmed")
      .setLanguage("English")
      .transform(x)
    var y = stemmer.groupBy("id_temp").agg(collect_list("terms_stemmed") as inputCol+"_stemmed")
    val df_alias = dataFrame.as("df_alias")
    val y_alias = y.as("y_alias")
    y = y_alias.join(df_alias, col("df_alias."+idCol)===col("y_alias.id_temp"))
    y = y.drop("id_temp").drop(inputCol)
    //Debugging
    Conf.debug(y, "Stemming")
    return y
  }

  /**
   * This function is used to tokenize the given dataframe.
   *
   * @param data: Dataframe to be tokenized
   * @param inputCol: Name of column to be tokenized
   * @param outputCol: Name of the new tokenized column
   * @return Dataframe Tokenized dataframe
   */
  def getTokenizedData(data: DataFrame, inputCol:String, outputCol:String): DataFrame ={

    val tokenizer = new Tokenizer()
    val tokenized = tokenizer.setInputCol(inputCol).setOutputCol(outputCol)
      .transform(data)
    Conf.debug(tokenized, "Tokenize Data")
    tokenized
  }

  def getNgramms(data: DataFrame, inputCol:String, outputCol:String, n: Int): DataFrame ={

    val ngrammExtractor = new NGram()
      .setN(n)
    val ngramms = ngrammExtractor.setInputCol(inputCol).setOutputCol(outputCol)
      .transform(data)
    Conf.debug(ngramms, s"Getting $n-gramms")
     ngramms
  }

  /**
   *
   * @param data: Dataframe to remove stopword from
   * @param inputCol: Name of column to remove stopwords from
   * @param outputCol: Name of the new column after stopword removal
   * @return The data without stopwords in dataframe format
   */
  def getStopWordRemovedData(data: DataFrame, inputCol: String, outputCol: String): DataFrame ={
    val stopWordsRemoved = new StopWordsRemover()
      .setInputCol(inputCol)
      .setOutputCol(outputCol)
      .transform(data)

    Conf.debug(stopWordsRemoved, "Stop words remove")
    return stopWordsRemoved
  }

  /**
   *
   * @param data: Dataframe for TF-IDF calculation
   * @param inputCol: Name of column for TF-IDF calculation
   * @param outputCol: Name of new column with input column's TF-IDF values
   * @return  Dataframe
   */
  def getTF_IDF(data: DataFrame, inputCol: String, outputCol: String): DataFrame ={

    val hashingTF = new HashingTF()
      .setInputCol(inputCol).setOutputCol(outputCol)

    val featurizedData = hashingTF.transform(data)//.select(features))

    val idf_titles = new IDF().setInputCol(outputCol).setOutputCol(outputCol+"_tf-idf")
    val idfModel = idf_titles.fit(featurizedData)

    val rescaledData = idfModel.transform(featurizedData)
    Conf.debug(rescaledData, "TF-IDF")
    return rescaledData
  }

  /**
   * A function to calculate the sqrt(norm) of a Vector. Needed to calculate cosine similarity
   *
   * @param vectorA: Vector
   * @return Double
   */
  def calcNorm(vectorA: SparseVector): Double = {
    var norm = 0.0
    for (i <-  vectorA.indices){ norm += vectorA(i)*vectorA(i) }
    math.sqrt(norm)
  }

  /**
   * A function to calculate Cosine Similarity of two Vectors
   *
   * @param vectorA: VectorA
   * @param vectorB: VectorB
   * @param normASqrt: Norm of VectorA
   * @param normBSqrt: Norm of VectorB
   * @return Cosine Similarity of given Vectors
   */
  def cosineSimilarity(vectorA: SparseVector,
                       vectorB:SparseVector,normASqrt:Double,normBSqrt:Double) : Double = {

    var dotProduct = 0.0
    for (i <-  vectorA.indices){
      dotProduct += vectorA(i) * vectorB(i)
    }
    val div = normASqrt * normBSqrt
    if (div == 0 )
      0
    else
      dotProduct / div
  }

  def jaccardSimilarity(column1: mutable.WrappedArray[String], column2: mutable.WrappedArray[String]): Double ={
    val similarity = column1.intersect(column2).size.toDouble / column1.union(column2).size
    similarity
  }

  def getCommonWordsNum(column1: mutable.WrappedArray[String], column2: mutable.WrappedArray[String]): Double ={
    var commonWords = 0.0
    if (column2.nonEmpty) {
      commonWords = column1.intersect(column2).size.toDouble / column2.size
    }
    commonWords
  }

  def relevanceConverter(relevance: Double) : Int = {
    var relevanceBinary = 1

    if (relevance < 1.5) {
      relevanceBinary = 0
    }
    relevanceBinary
  }

  def replaceNullValues(sim_title: Double, sim_desc: Double, sim_title_bi: Double,
                        sim_desc_bi: Double, sim_attr: Double): Double= {

    val mean_sim = (sim_title + sim_desc + sim_title_bi + sim_desc_bi)/4
    var sim_not_null = sim_attr
    if (sim_attr == -1.0) {
      sim_not_null = mean_sim
    }
    sim_not_null
  }

  /**
   *
   * @param dataFrame Data with many feature columns
   * @param featureCol Array tha contains the name of the columns to assemble to one
   * @return Dataframe with assembled features to one columns
   */
  def assembler(dataFrame: DataFrame, featureCol:Array[String]): DataFrame ={
    val assembler = new VectorAssembler()
      .setInputCols(featureCol)
      .setOutputCol("featuresFinal")
    val data = assembler.transform(dataFrame)
    data
  }

  /**
   *
   * @param dataFrame
   * @return
   */
  def pca(dataFrame: DataFrame): DataFrame ={
    val pca = new PCA()
      .setInputCol("featuresFinal")
      .setOutputCol("pcaFeatures")
      .setK(9)
      .fit(dataFrame)

    val result = pca.transform(dataFrame)
    return result
  }
}
