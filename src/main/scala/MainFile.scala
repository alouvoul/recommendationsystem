import org.apache.spark.ml.linalg.SparseVector
import org.apache.spark.sql.functions._
import Utilities.{calcNorm, cosineSimilarity, getCommonWordsNum, jaccardSimilarity}
import SupervisedML.{getDecisionTreeRegressionMSE, getGradientBoostedTreeRegressionMSE, getLinearRegressionMSE, getRandomForestRegressionMSE}
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.storage.StorageLevel

import scala.collection.mutable

/**
 * Step 1: Read files
 * Step 2: Preprocessing
 * ----- Tokenize data
 * ----- StopWords Removal
 * ----- Stemming data
 * ----- Remove/Edit punctuations
 * ----- Final Tokenization
 * Step 3: TF-IDF
 * Step 4: Similarities
 * Step 5: Algorithms
 */
object MainFile {

  def main(args: Array[String]): Unit = {

    //==================================================================================================//
    //====================================== Configurations ===========================================//
    val spark = Conf.sparkConfigurations()
    var data1: DataFrame = null
    if(!FileHandling.fileExist(Conf.filename_for_preprocessing_save_results)){

      //==================================================================================================//
      //====================================== Reading Phase 1 ===========================================//
      /**
       * Loading Dataset
       */
      val trainFile = FileHandling.fileRead("train.csv", spark)
      val descriptionFile = FileHandling.fileRead("product_descriptions.csv", spark)
      val attributeFile = FileHandling.attributeFileRead("attributes.csv", spark)

      // xtrain is a dataframe with three columns "id", "product_title" and "search_term"
      val xtrain = trainFile.select("id", "product_title", "search_term", "product_uid")

      //ytrain is a dataframe with two columns, "id" and the target value "relevance"
      val ytrain = trainFile.select("id", "relevance", "product_uid")


      //==================================================================================================//
      //====================================== Preprocessing Phase 2 ======================================//

      /**
       * Tokenize data
       */
      var title = Utilities.getTokenizedData(xtrain, "product_title", "product_title_tokens")
        .select("id", "product_title_tokens", "product_uid")
      var search_term = Utilities.getTokenizedData(xtrain, "search_term", "search_term_tokens")
          .select("id", "search_term_tokens", "product_uid")

      var description = Utilities.getTokenizedData(descriptionFile, "product_description", "product_description_tokens")
      var attribute = Utilities.getTokenizedData(attributeFile, "attributes", "attributes_tokens")

      /**
       * Stem the sentences
       */
      title = Utilities.getStemmedData(title, "id", "product_title_tokens")
      search_term = Utilities.getStemmedData(search_term, "id", "search_term_tokens")
      description = Utilities.getStemmedData(description, "product_uid", "product_description_tokens")
      attribute = Utilities.getStemmedData(attribute, "product_uid", "attributes_tokens")

      /**
       * Remove punctuations
       */
      title = Utilities.getRemovedPunctuationData(title.select("id", "product_title_tokens_stemmed", "product_uid"), spark, Array("id", "product_title", "product_uid"))
      search_term = Utilities.getRemovedPunctuationData(search_term.select("id", "search_term_tokens_stemmed", "product_uid"), spark, Array("id", "search_term", "product_uid"))
      description = Utilities.getRemovedPunctuationData(
        description.select("product_uid", "product_description_tokens_stemmed", "product_description"), spark, Array("product_uid", "product_description", "description_old")
      ).drop("description_old")
      attribute = Utilities.getRemovedPunctuationData(
        attribute.select("product_uid", "attributes_tokens_stemmed", "attributes"), spark, Array("product_uid","attributes", "attributes_old")
      )
      // Attributes need to be parsed a second time
      attribute = Utilities.getRemovedPunctuationData(
        attribute.select("product_uid", "attributes", "attributes_old"), spark, Array("product_uid","attributes", "attributes_old")).drop("attributes_old")

      /**
       * Tokenizing the final sting that occurred after preprocessing
       */
      val title_preprocessed = Utilities.getTokenizedData(title, "product_title", "product_title_tokenized")
      val search_term_preprocessed = Utilities.getTokenizedData(search_term, "search_term", "search_term_tokenized")
      val description_preprocessed = Utilities.getTokenizedData(description, "product_description", "description_tokenized")
      val attributes_preprocessed = Utilities.getTokenizedData(attribute, "attributes", "attributes_tokenized")


      /**
       * Stopwords Removal
       */
      val title_tokenized_preprocessed = Utilities.getStopWordRemovedData(title_preprocessed, "product_title_tokenized", "product_title_parsed")
      val search_term_tokenized_preprocessed = Utilities.getStopWordRemovedData(search_term_preprocessed, "search_term_tokenized", "search_term_tokens_parsed")
      val description_tokenized_preprocessed = Utilities.getStopWordRemovedData(description_preprocessed, "description_tokenized", "product_description_parsed")
      val attribute_tokenized_preprocessed = Utilities.getStopWordRemovedData(attributes_preprocessed, "attributes_tokenized", "attributes_parsed")


      /**
       * Getting Bigramms
       */
      val title_bigramms = Utilities.getNgramms(title_tokenized_preprocessed, "product_title_parsed", "product_title_bigramms", 2)
      val search_term_bigramms = Utilities.getNgramms(search_term_tokenized_preprocessed, "search_term_tokens_parsed", "search_term_bigramms", 2)
      val description_bigramms = Utilities.getNgramms(description_tokenized_preprocessed, "product_description_parsed", "product_description_bigramms", 2)
      val attribute_bigramms = Utilities.getNgramms(attribute_tokenized_preprocessed, "attributes_parsed", "attributes_bigramms", 2)




      //==================================================================================================//
      //====================================== TF-IDF Phase 3 ============================================//

      /**
       * TF-IDF feature extraction
       */
      val rescaledData_titles = Utilities.getTF_IDF(title_tokenized_preprocessed.select("id", "product_uid", "product_title_parsed"), "product_title_parsed", "product_titles")
      val rescaledData_search = Utilities.getTF_IDF(search_term_tokenized_preprocessed.select("id", "product_uid", "search_term_tokens_parsed"), "search_term_tokens_parsed", "search_term")
      val rescaledData_description = Utilities.getTF_IDF(description_tokenized_preprocessed.select("product_uid", "product_description_parsed"), "product_description_parsed", "description")
      val rescaledData_attribute = Utilities.getTF_IDF(attribute_tokenized_preprocessed.select("product_uid", "attributes_parsed"), "attributes_parsed", "attributes")

      /**
       * TF-IDF of Bigramms
       */
      val rescaledData_titles_bigramms = Utilities.getTF_IDF(title_bigramms.select("id", "product_uid", "product_title_bigramms"), "product_title_bigramms", "product_titles_bi")
      val rescaledData_search_term_bigramms = Utilities.getTF_IDF(search_term_bigramms.select("id", "product_uid", "search_term_bigramms"), "search_term_bigramms", "search_term_bi")
      val rescaledData_description_bigramms = Utilities.getTF_IDF(description_bigramms.select("product_uid", "product_description_bigramms"), "product_description_bigramms", "description_bi")
      val rescaledData_attribute_bigramms = Utilities.getTF_IDF(attribute_bigramms.select("product_uid", "attributes_bigramms"), "attributes_bigramms", "attributes_bi")


      /**
       * With the User Defined Functions (UDFs) below we can apply the Utilities function to a whole column of a Dataframe
       */
      val calcNormDF = udf[Double,SparseVector](calcNorm)
      val cosineSimilarityDF = udf[Double, SparseVector, SparseVector, Double, Double](cosineSimilarity)
      val jaccard = udf[Double, mutable.WrappedArray[String], mutable.WrappedArray[String]](jaccardSimilarity)
      val commonWords = udf[Double, mutable.WrappedArray[String], mutable.WrappedArray[String]](getCommonWordsNum)


      /**
       * Calculating each Vector's norms
       */
      val normalized_titles = rescaledData_titles.withColumn("norm_titles",calcNormDF(col("product_titles_tf-idf")))
      val normalized_search = rescaledData_search.withColumn("norm_search",calcNormDF(col("search_term_tf-idf")))
      val normalized_description = rescaledData_description.withColumn("norm_description",calcNormDF(col("description_tf-idf")))
      val normalized_attribute = rescaledData_attribute.withColumn("norm_attributes",calcNormDF(col("attributes_tf-idf")))

      /**
       * Calculating Bigramms norms
       */
      val normalized_titles_bi = rescaledData_titles_bigramms.withColumn("norm_titles_bi",calcNormDF(col("product_titles_bi_tf-idf")))
      val normalized_search_bi = rescaledData_search_term_bigramms.withColumn("norm_search_bi",calcNormDF(col("search_term_bi_tf-idf")))
      val normalized_description_bi = rescaledData_description_bigramms.withColumn("norm_description_bi",calcNormDF(col("description_bi_tf-idf")))
      val normalized_attribute_bi = rescaledData_attribute_bigramms.withColumn("norm_attributes_bi",calcNormDF(col("attributes_bi_tf-idf")))


      /**
       * Defining aliases for table joins
       */

      /**
       * The dataframes with TF-IDF values
       */
      val df_titles = rescaledData_titles.as("dftitles")
      val df_search = rescaledData_search.as("dfsearch")
      val df_description = rescaledData_description.as("dfdescription")
      val df_attribute = rescaledData_attribute.as("dfattribute")

      val df_titles_bi = rescaledData_titles_bigramms.as("df_titles_bi")
      val df_search_bi = rescaledData_search_term_bigramms.as("df_search_bi")
      val df_description_bi = rescaledData_description_bigramms.as("df_description_bi")
      val df_attribute_bi = rescaledData_attribute_bigramms.as("df_attributes_bi")


      /**
       * The dataframes with the norm values
       */
      val df_normalized_titles = normalized_titles.as("df_normalized_titles")
      val df_normalized_search = normalized_search.as("df_normalized_search")
      val df_normalized_description = normalized_description.as("df_normalized_description")
      val df_normalized_attribute = normalized_attribute.as("df_normalized_attribute")

      val df_norm_titles_bi = normalized_titles_bi.as("df_norm_titles_bi")
      val df_norm_search_bi = normalized_search_bi.as("df_norm_search_bi")
      val df_norm_description_bi = normalized_description_bi.as("df_norm_description_bi")
      val df_norm_attribute_bi = normalized_attribute_bi.as("df_norm_attribute_bi")

      /**
       * The dataframes with the tokenized words from which we can calculate the jaccard similarities
       */

      val title_token_jaccard = title_tokenized_preprocessed.as("title_token_jaccard")
      val search_token_jaccard = search_term_tokenized_preprocessed.as("search_token_jaccard")
      val description_token_jaccard = description_tokenized_preprocessed.as("description_token_jaccard")
//      val attributes_token_jaccard = attribute_tokenized_preprocessed.as("attributes_token_jaccard")


      //==================================================================================================//
      //====================================== Similarities Phase 4 ======================================//
      /**
       * Calculating the Cosine Similarity of the search term and the product title
       * */
      val similarity_search_term_with_title = df_titles.join(df_search, col("dftitles.id") === col("dfsearch.id"))
        .join(df_normalized_titles, col("dftitles.id") === col("df_normalized_titles.id"))
        .join(df_normalized_search, col("dftitles.id") === col("df_normalized_search.id"))
        .withColumn("similarity1", cosineSimilarityDF(col("dftitles.product_titles_tf-idf"),
          col("dfsearch.search_term_tf-idf"), col("df_normalized_titles.norm_titles"),
          col("df_normalized_search.norm_search"))).select("dftitles.id", "dftitles.product_uid", "similarity1")


      /**
       * Calculating the Cosine Similarity of the search term and the description of the product
       * */
      val similarity_search_term_with_description = df_search.join(
        df_description, col("dfdescription.product_uid") === col("dfsearch.product_uid"), "leftouter")
        .join(df_normalized_description, col("dfdescription.product_uid") === col("df_normalized_description.product_uid"), "leftouter")
        .join(df_normalized_search, col("dfsearch.id") === col("df_normalized_search.id"))
        .withColumn("similarity2", cosineSimilarityDF(col("dfdescription.description_tf-idf"),
          col("dfsearch.search_term_tf-idf"), col("df_normalized_description.norm_description"),
          col("df_normalized_search.norm_search"))).select("dfsearch.id","dfsearch.product_uid", "similarity2")

      /**
       * Calculating the Cosine Similarity of the search term and the attributes of the product
       * */
      val similarity_search_term_with_attributes = df_search.join(
        df_attribute, col("dfattribute.product_uid") === col("dfsearch.product_uid"), "leftouter")
        .join(df_normalized_attribute, col("dfattribute.product_uid") === col("df_normalized_attribute.product_uid"), "leftouter")
        .join(df_normalized_search, col("dfsearch.id") === col("df_normalized_search.id"))
        .withColumn("similarity3", cosineSimilarityDF(col("dfattribute.attributes_tf-idf"),
          col("dfsearch.search_term_tf-idf"), col("df_normalized_attribute.norm_attributes"),
          col("df_normalized_search.norm_search"))).select("dfsearch.id","dfsearch.product_uid", "similarity3")

      /**
       * Calculating the Cosine Similarity of the search term and the product title: BIGRAMMS
       */

      val similarity_search_term_with_title_bi = df_titles_bi.join(df_search_bi, col("df_titles_bi.id") === col("df_search_bi.id"))
        .join(df_norm_titles_bi, col("df_titles_bi.id") === col("df_norm_titles_bi.id"))
        .join(df_norm_search_bi, col("df_titles_bi.id") === col("df_norm_search_bi.id"))
        .withColumn("similarity4", cosineSimilarityDF(col("df_titles_bi.product_titles_bi_tf-idf"),
          col("df_search_bi.search_term_bi_tf-idf"), col("df_norm_titles_bi.norm_titles_bi"),
          col("df_norm_search_bi.norm_search_bi"))).select("df_titles_bi.id", "df_titles_bi.product_uid", "similarity4")

      /**
       * Calculating the Cosine Similarity of the search term and the description of the product: BIGRAMMS
       * */
      val similarity_search_term_with_description_bi = df_search_bi.join(
        df_description_bi, col("df_description_bi.product_uid") === col("df_search_bi.product_uid"), "leftouter")
        .join(df_norm_description_bi, col("df_description_bi.product_uid") === col("df_norm_description_bi.product_uid"), "leftouter")
        .join(df_norm_search_bi, col("df_search_bi.id") === col("df_norm_search_bi.id"))
        .withColumn("similarity5", cosineSimilarityDF(col("df_description_bi.description_bi_tf-idf"),
          col("df_search_bi.search_term_bi_tf-idf"), col("df_norm_description_bi.norm_description_bi"),
          col("df_norm_search_bi.norm_search_bi"))).select("df_search_bi.id","df_search_bi.product_uid", "similarity5")

      /**
       * Calculating the Cosine Similarity of the search term and the attributes of the product: BIGRAMMS
       * */
      val similarity_search_term_with_attributes_bi = df_search_bi.join(
        df_attribute_bi, col("df_attributes_bi.product_uid") === col("df_search_bi.product_uid"), "leftouter")
        .join(df_norm_attribute_bi, col("df_attributes_bi.product_uid") === col("df_norm_attribute_bi.product_uid"), "leftouter")
        .join(df_norm_search_bi, col("df_search_bi.id") === col("df_norm_search_bi.id"))
        .withColumn("similarity6", cosineSimilarityDF(col("df_attributes_bi.attributes_bi_tf-idf"),
          col("df_search_bi.search_term_bi_tf-idf"), col("df_norm_attribute_bi.norm_attributes_bi"),
          col("df_norm_search_bi.norm_search_bi"))).select("df_search_bi.id","df_search_bi.product_uid", "similarity6")

      /**
       *  Jaccard similarity of search term and title
       */
      var title_search_jaccard_sim = title_token_jaccard.join(search_token_jaccard,
        col("title_token_jaccard.id") === col("search_token_jaccard.id")).withColumn("similarity7",
        jaccard(col("title_token_jaccard.product_title_parsed"), col("search_token_jaccard.search_term_tokens_parsed")))

      title_search_jaccard_sim = title_search_jaccard_sim.withColumn("common_words1",
        commonWords(col("title_token_jaccard.product_title_parsed"), col("search_token_jaccard.search_term_tokens_parsed")))
        .select("title_token_jaccard.id", "title_token_jaccard.product_uid", "similarity7", "common_words1")

      /**
       *  Jaccard similarity of search term and description
       */
      var description_search_jaccard_sim = search_token_jaccard.join(description_token_jaccard,
        col("search_token_jaccard.product_uid") === col("description_token_jaccard.product_uid"), "leftouter")
        .withColumn("similarity8", jaccard(col("search_token_jaccard.search_term_tokens_parsed"),
            col("product_description_parsed")))

      description_search_jaccard_sim = description_search_jaccard_sim.withColumn("common_words2",
        commonWords(col("product_description_parsed"), col("search_token_jaccard.search_term_tokens_parsed")))
        .select("search_token_jaccard.id","search_token_jaccard.product_uid", "similarity8", "common_words2")


      /**
       * Creating the dataset.
       */

      // Join description and product_title similarity
      val sim = similarity_search_term_with_description.join(similarity_search_term_with_title, col("dftitles.id") === col("dfsearch.id"))
        .select("dftitles.id","similarity1", "similarity2")

      // ADD relevance score to the array
      val data = sim.join(ytrain, sim("dftitles.id") === ytrain("id")).select("dftitles.id", "similarity1", "similarity2", "relevance", "product_uid")

      data1 = data.as("dfdata")

      // ADD attribute similarity
      data1 = data1.join(similarity_search_term_with_attributes, col("dfsearch.id")===col("dfdata.id"), "leftouter")
        .select("dfdata.id", "similarity1", "similarity2", "similarity3", "relevance")

      // ADD bigramm product_title similarity
      data1 = data1.join(similarity_search_term_with_title_bi, col("df_titles_bi.id") === col("dfdata.id"))
        .select("dfdata.id", "similarity1", "similarity2", "similarity3", "similarity4", "relevance")

      // ADD bigramm description similarity
      data1 = data1.join(similarity_search_term_with_description_bi, col("df_search_bi.id") === col("dfdata.id"))
        .select("dfdata.id", "similarity1", "similarity2", "similarity3", "similarity4", "similarity5", "relevance")

      // ADD bigramm attribute similarity
      data1 = data1.join(similarity_search_term_with_attributes_bi, col("df_search_bi.id") === col("dfdata.id"))
        .select("dfdata.id", "similarity1", "similarity2", "similarity3", "similarity4", "similarity5", "similarity6", "relevance")

      // ADD jaccard similarity of search term and product title
      data1 = data1.join(title_search_jaccard_sim, col("title_token_jaccard.id") === col("dfdata.id"))
          .select("dfdata.id", "similarity1", "similarity2", "similarity3",
            "similarity4", "similarity5", "similarity6", "similarity7", "common_words1", "relevance")

      // ADD jaccard similarity of search term and product description
      data1 = data1.join(description_search_jaccard_sim, col("search_token_jaccard.id")===col("dfdata.id"), "leftouter")
        .select("dfdata.id", "similarity1", "similarity2", "similarity3",
          "similarity4", "similarity5", "similarity6", "similarity7", "similarity8", "common_words1", "common_words2", "relevance")


      /**
       * Attribute file doesn't contain all product_id and there are null values.
       * NULL values replaced by the mean of the other similarities
       */

      val replaceNullValues = udf[Double, Double, Double, Double, Double, Double](Utilities.replaceNullValues)

      // Replacing null values of similarity3
      data1 = data1.na.fill(-1.0, Array("similarity3"))
      data1 = data1.withColumn("sim_3_not_null", replaceNullValues(col("similarity1"),
        col("similarity2"), col("similarity4"), col("similarity5"), col("similarity3")))

      // Replacing null values of similarity6
      data1 = data1.na.fill(-1.0, Array("similarity6"))
      data1 = data1.withColumn("sim_6_not_null", replaceNullValues(col("similarity1"),
        col("similarity2"), col("similarity4"), col("similarity5"), col("similarity6")))
      data1 = data1.drop("similarity3").drop("similarity6")

      FileHandling.writeFile(data1, Conf.filename_for_preprocessing_save_results)
    }
    data1 = FileHandling.readParquet(Conf.filename_for_preprocessing_save_results, spark)
    data1.persist(StorageLevel.MEMORY_ONLY_SER)

    //==================================================================================================//
    //====================================== Algorithms Phase 5 ========================================//


    /**
     *  Get the MSE using Linear Regression
     */
    val mse_LinearRegression = getLinearRegressionMSE(data1, Array("similarity1", "similarity2", "sim_3_not_null",
    "similarity4", "similarity5", "sim_6_not_null", "similarity7", "similarity8", "common_words1", "common_words2"), "relevance")
    println(s"Mean Squared Error (MSE) on test data for Linear Regression = $mse_LinearRegression")

    /**
     *  Get the MSE using Decision Tree Regression
     */
    val mse_decision_tree = getDecisionTreeRegressionMSE(data1, Array("similarity1", "similarity2", "sim_3_not_null",
      "similarity4", "similarity5", "sim_6_not_null", "similarity7", "similarity8", "common_words1", "common_words2"), "relevance")
    println(s"Mean Squared Error (MSE) on test data for Decision Tree Regression = $mse_decision_tree")

    val mse_random_forest = getRandomForestRegressionMSE(data1, Array("similarity1", "similarity2", "sim_3_not_null",
      "similarity4", "similarity5", "sim_6_not_null", "similarity7", "similarity8", "common_words1", "common_words2"), "relevance")
    println(s"Mean Squared Error (MSE) on test data for Random Forest Regression = $mse_random_forest")

    val mse_gradient_boosted_tree = getGradientBoostedTreeRegressionMSE(data1, Array("similarity1", "similarity2", "sim_3_not_null",
      "similarity4", "similarity5", "sim_6_not_null", "similarity7", "similarity8", "common_words1", "common_words2"), "relevance")
    println(s"Mean Squared Error (MSE) on test data for Gradient-Boosted Tree Regression = $mse_gradient_boosted_tree")


    //==================================================================================================//
    //====================================== Unsuprervised Approach ====================================//

    /**
     *  A UDF to convert the relevance column for binary classification
     */
    val relevanceConverter = udf[Int, Double](Utilities.relevanceConverter)
    var cas_data = data1.withColumn("relevance", data1.col("relevance").cast(DoubleType))

    /**
     *  We add a new column "class" to the dataframe. For every row the value of class is 1 if relevance >= 1.5 and
     *  0 if relevance < 1.5
     */
    cas_data = cas_data.withColumn("class", relevanceConverter(cas_data.col("relevance")))

    val f1_kmeans = UnsupervisedML.getKmeansScore(cas_data, Array("similarity1", "similarity2", "sim_3_not_null",
      "similarity4", "similarity5", "sim_6_not_null"))
    println(s"F1 score on test data for K-Means clustering = $f1_kmeans")

    val f1_bkmeans = UnsupervisedML.bisectingKMeans(cas_data, Array("similarity7", "similarity8", "common_words1"))
    println(s"F1 score on test data for Bisecting K-Means clustering = $f1_bkmeans")
  }

}
