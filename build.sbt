name := "BDProject"

version := "0.1"

scalaVersion := "2.11.8"

val sparkVersion = "2.4.4"

resolvers ++= Seq("spark-stemming" at "https://dl.bintray.com/spark-packages/maven/")

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "org.apache.spark" %% "spark-mllib" % sparkVersion,
  //"org.apache.spark" %% "spark-streaming" % sparkVersion,
  //"org.apache.spark" %% "spark-hive" % sparkVersion
  "master" % "spark-stemming" % "0.1.1"
)

